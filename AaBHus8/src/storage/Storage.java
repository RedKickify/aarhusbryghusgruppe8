package storage;

import java.util.ArrayList;
import java.util.HashMap;

import application.model.PriceCategory;
import application.model.Product;
import application.model.Sale;

public class Storage {
	
	private String name;
	
	HashMap<String, PriceCategory> priceCategories = new HashMap<String, PriceCategory>();
	HashMap<String, ArrayList<Product>> products = new HashMap<String, ArrayList<Product>>();
	
	ArrayList<Sale> sales = new ArrayList<Sale>();
	ArrayList<Sale> commencedSales = new ArrayList<Sale>();
	
	
// Get metoder
	public String getName() {
		return name;
	}
	public HashMap<String, PriceCategory> getPriceCategories() {
		return new HashMap<String, PriceCategory>(priceCategories);
	}
	public HashMap<String, ArrayList<Product>> getProducts() {
		return new HashMap<String, ArrayList<Product>>(products);
	}
	public ArrayList<Sale> getSales() {
		return new ArrayList<Sale>(sales);
	}
	public ArrayList<Sale> getCommencedSales() {
		return new ArrayList<Sale>(commencedSales);
	}
	
// Create metoder
	public PriceCategory createPriceCategory(String name) {
		PriceCategory nyPriceCategory = new PriceCategory(name);
		
		priceCategories.put(name, nyPriceCategory);
		
		return nyPriceCategory;
	}
	
	
	// skal der være create arraylist af products eller create product ?           tager vi senere
	
	public Sale createSale() {
		Sale nySale = new Sale();
		
	//evt bestemme hvilken arraylist man vil have den på? sales eller commencedSales
		
		return nySale;
	}
	
	
// Add metoder
	
	
	
	
	
	
}
