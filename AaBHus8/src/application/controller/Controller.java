package application.controller;

import application.controller.Controller;
import storage.Storage;

public class Controller {
	
	private Storage storage;
	private static Controller controller;

	private Controller() {
		storage = new Storage();
	}

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}
	
	
	
	
}
