package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Rental {
	private LocalDateTime rentStart;
	private LocalDateTime rentEnd;
	private ArrayList<Depositable> returnedDepositables = new ArrayList<>();
	
	public Rental(LocalDateTime rentStart, LocalDateTime rentEnd) {
		this.rentStart = rentStart;
		this.rentEnd = rentEnd;
	}
	
	public void addReturnedDepositable(Depositable depositable) {
		returnedDepositables.add(depositable);
	}

	public LocalDateTime getRentStart() {
		return rentStart;
	}

	public LocalDateTime getRentEnd() {
		return rentEnd;
	}

	public ArrayList<Depositable> getReturnedDepositables() {
		return returnedDepositables;
	}
}
