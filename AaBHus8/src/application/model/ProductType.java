package application.model;

public enum ProductType {
	BOTTLE,
	DRAFTBEER,
	SPIRIT,
	KEG,
	CO2,
	CLOTHING,
	TAPS,
	GLASS,
	PACKAGE

}
