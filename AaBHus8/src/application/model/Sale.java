package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Sale {
	private ArrayList<ProductSale> productSales = new ArrayList<>();
	private PaymentMethod paymentMethod;
	private LocalDateTime dateTime;
	
	public Sale() {
		dateTime = LocalDateTime.now();
	}
	
	public void createNewProductSale(Product product, int amount, double price) {
		ProductSale productSale = new ProductSale(product, amount, price);
		productSales.add(productSale);
	}
	
	public double calculateTotalPrice() {
		double result = 0;
		
		for(int i = 0; i < productSales.size(); i++) {
			result += productSales.get(i).calculateTotalPrice();
		}
		
		return result;
	}
	
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public ArrayList<ProductSale> getProductSales() {
		return productSales;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}
}
