package application.model;

public class Depositable extends Product {
	private double amount;
	private boolean isUsed;

	public Depositable(String name, double amount, ProductType productType) {
		super(name, productType);
		this.amount = amount;
		this.isUsed = false;
	}

	public double getAmount() {
		return amount;
	}

	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}
	
	
	
	

}
