package application.model;

public enum PaymentMethod {
	MOBILEPAY, CREDITCARD, CASH, PUNCHCARD
}
