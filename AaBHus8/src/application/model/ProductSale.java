package application.model;

public class ProductSale {
	private Product product;
	private int amount;
	private double unitPrice;
	
	public ProductSale(Product product, int amount, double unitPrice) {
		this.product = product;
		this.amount = amount;
		this.unitPrice = unitPrice;
	}
	
	public double calculateTotalPrice() {
		return amount * unitPrice;
	}

	public Product getProduct() {
		return product;
	}

	public int getAmount() {
		return amount;
	}

	public double getUnitPrice() {
		return unitPrice;
	}
	
	
}
