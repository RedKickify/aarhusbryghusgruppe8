package application.model;

import java.time.LocalDateTime;

public class Tour extends Sale {
	private LocalDateTime tourDateTime;
	
	public Tour(LocalDateTime tourDateTime) {
		this.tourDateTime = tourDateTime;
	}

	public LocalDateTime getTourDateTime() {
		return tourDateTime;
	}
}
