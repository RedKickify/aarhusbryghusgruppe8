package application.model;

public class Product {
	private String name;
	private ProductType productType;

	public Product(String name, ProductType productType) {
		super();
		this.name = name;
		this.productType = productType;
	}

	public String getName() {
		return name;
	}

	public ProductType getProductType() {
		return productType;
	}
	
}
