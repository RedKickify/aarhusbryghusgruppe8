package application.model;

import java.util.HashMap;
import java.util.Map;


public class PriceCategory {
	private Map<Product, Double> prices;
	private String name;

	public PriceCategory(String name) {
		super();
		this.name = name;
		prices = new HashMap<Product, Double>();
	}
	
	public String getName() {
		return name;
	}
	public void addProduct(Product product, double price) {
		prices.put(product, price);
	}
	public void setPrice(Product product, double price) {
		prices.put(product, price);
	}
	public void removeProduct(Product product) {
		prices.remove(product);
	}
	public HashMap<Product, Double> getPrices() {
		return new HashMap<>(prices);
	}
	public double getPriceOfProduct(Product product) {
		return prices.get(product);
	}
	
	@Override
	public String toString() {
		return name;
	}
	

}
